 


# Lesson 1 - Getting started with Python: Hello World

## What is a computer?
A computer is a device that stores and manipulates information. That's it! They just happen to be able to store an awful lot of it, and manipulate it very quickly. At least modern electronic computers are able to. 

Computers don't always have to be electronic. In 1822 Charles Babbage created the Difference Engine - a mechanical computer that operated by crank: 

<iframe width="560" height="315" src="https://www.youtube.com/embed/be1EM3gQkAY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

https://en.wikipedia.org/wiki/Difference_engine

By today's standards this was a very slow computer. A modern PC can do roughly a 300 billion calculations per second, where the Difference engine can do about 10 if crank it very quickly.

### How can we describe things as information?
For a computer to be able to do something interesting for us, the problem must be described in a way that the computer can work with it. So, we need to be able to describe our task with numbers, sequences of letters, or at the most basic level, __True__ and __False__, __1__ or __0__. Figuring out how to do this and knowing what to ask the computer to do are the most important part of programming.

Fortunately, modern programming languages like Python make this pretty easy and provide a lot pre-built of tools to make your programs do useful tasks.

## Opening PyCharm
PyCharm is the editor that we will be using. To start it up, double click its icon on the desktop

![](2019-07-27-14-46-10.png)

## Editing Code
Within the PyCharm editor, you can move the cursor (the little blinking vertical line) with the arrow keys. If you want to move around the code very quickly, you can hold in the **CTRL** button while hitting **Left** or **Right** to move a whole word at a time. 

## Hello World!
It's traditional for a person's first program in a new programming language to be a simple "Hello World". This program will do one thing - print "Hello World" on the screen.
Improvisation is encouraged.
```python
print("Greetings Human!")
```

### Running your Program
Run your program by clicking the green arrow in the top right of the screen:

![](2019-07-27-15-06-46.png)

And you should see a result appear in a window at the bottom of the screen.

![](2019-07-27-15-07-55.png)

### The Python Console
Sometimes, when you are writing a program, it's helpful to play with the code interactively. That is, you can write a program one line at a time, and have it run as you finish each line.

To open the python console, go to the Tools menu at the top of the screen, and open the `Python Console...`
![](2019-07-27-15-10-29.png )

Then, at the bottom of the screen, a tab will open for your Python Console:
![](2019-07-27-15-14-10.png)
If you click there to move your cursor to green `>>>` prompt, you can type in lines of Python and see the results.

if you press the up or down arrows you can bring back previous lines that you've written and edit them or rerun them.

### Fun with Strings
We've just used the `print` command to make a small program that prints a few words out on the screen. In Python, and in most other languages, these words, usually written with double or single quotes, e.g. "A string", or 'another string', are called **strings**. So, when we wrote `print("Hello World")`, we were passing a **string**, "Hello World", to the `print` function.

`print` can print numbers, multiple strings a time too.

```python
print("one string", "another string", 12345)
one string another string 12345
```

Strings can also be 'stuck together' or 'added' with the + sign:

```python
"Hello" + " there"
'Hello there'
```

and multiplied with the asterisk *

```python
"Cowabunga" * 10
'CowabungaCowabungaCowabungaCowabungaCowabungaCowabungaCowabungaCowabungaCowabungaCowabunga'
```

### Variables
Most programs have to deal with a lot of data. We need to have some way to tell the computer to store information and look it up when we need it. For this, we use **variables**. You can think of them as a box with a nametag on it. You can put whatever 'information' you want in that box, get it out, or put something else in the box.

For example.

```python
x = 10
y = 5
z = x + y
print(z) # z is equal to 10 + 5, since those are what x and y contain
15
x = 2 # Then, we can replace the value in the variable x with a different number. 2
z = x + y # y is still the same
print(z) # so z becomes 5 + 2, or 7
7
```

Of course, strings can also be stored in variables
```python
message = "I like pizza\n" # Note - the \n is a special code to print a newline
repeats = 5
print(message * repeats)
message = "And also, asparagus. "
repeats = 2
print(message * repeats)

I like pizza
I like pizza
I like pizza
I like pizza
I like pizza
And also, asparagus. And also, asparagus. 
```

### Input from the user
Variables are useful, but a program would be pretty boring if it had to do the same thing every time. Sometimes we need to get feedback from a person, or perhaps even another program. To do that, there is the `input()` command. It's a way to ask the user of your program a question. Note that the user does not need to give a correct answer.

For example

```python
question = "what is the capital of Pennsylvania"
answer = input(question)
print("The answer to the question:", question, "is:", answer)


what is the capital of Pennsylvania>? Harrisburger with fries
The answer to the question: what is the capital of Pennsylvania is: Harrisburger with fries
```

`input` can be called with a string passed to it, between the parenthesis `()`. It will print the question out for the user, and then, once the user types in their answer, and hits Enter, `input()` will return the string that the user typed. You can put it in a variable e.g. `x = input()`, or print it `print(input('type something'))` or do anything else that you want with it.


### Integers and other Types
In the last section we learned that `input()` returns a string, or text. What if we want to type in a number, like 25? `input()` will return the string `"25"`, and not the number.

```python
input("Enter a number") * 10
Enter a number>? 25
'25252525252525252525'
```
Rather than resulting in 250, this just printed `"25"` ten times. Not quite what we wanted.

To convert a string into a number, so that you can do arithmetic with it, you use the `int(somestring)` command. What does `int` mean? It's short for Integer, which is more or less a Whole number, like 1, 100, or -2 but not a fraction or decimal like ½ or 0.5. Lots more information at https://en.wikipedia.org/wiki/Integer

So, to get Python to do some math for us, lets use `int`

```python
int(input("Enter a number")) * 10
Enter a number>? 7
70
```
That's more like it!

Integers and Strings are both **types** of data that Python can work with. There are many other types too.

In the right side of the python console, you can see all of the info that is being stored, along with their types:
![](2019-07-27-16-05-20.png)

You can even ask python directly about what **type** a bit of data is with the `type()` function.

```python
print("The type of message is:", type(message))
The type of message is: <class 'str'>
x = 2
print("the type of x is:", type(x))
the type of x is: <class 'int'>
```


